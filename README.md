# Crossref rate-limit tester

## Preamble

Crossref gets lots of questions about rate limits on our [REST API](https://api.crossref.org)- mostly from people doubting whether we are implimenting them as advertised. This script will help you answer those questions by allowing you to excercise the API at different rate limits and to see exactly how many of your requests are being rate-limited.

And, of course, the script is open source so you can make sure we aren't cheating here either. Y'all have trust issues don'tcha?

But first note that when we say that our API supprts a rate limit of `X`- we are referring to the *request rate* not the *response rate*. You may be able to acheive `50` requests per second, but it may take you `120` seconds or longer to retreive the responses from those requests. Why is this? Well-  because the API supports a wide variety of filters and options that can be combined in various ways. The more of these you use, the slower your requests are going to be. Not only that, but in many cases the complexity of your reqeuests will make your results *worse*- particularly in use-cases where you are trying to match references to a DOI. To understand a little bit more about how to optimise your requests to the API, have a gander at our [Tips for using the Crossref REST API](https://github.com/CrossRef/rest-api-doc/blob/master/api_tips.md)

## TODO

- add option to add trace id to user-agent header for easier tracking in logs

## Requirements

This shouldn't need saying anymore but- Python3 only.

## Installation

Do the usual incovation (in a virtual env or otherwise)

```bash
pip3 -m venv venv
. venv/bin/activate
pip3 install -r requirements
```

## Switches

- `-v` be verbose
- `-s` starting number of concurrent requests
- `-e` ending number of concurrent requests
- `-i` amount to increment concurrent requests
- `-u` URL to test against
- `-p` email address (Be "polite" by including this in user-agent header)
- `--use-plus` uses Plus token set in environvental variable `CROSSREF_API_TOKEN`. Exits if environmental variable not set.

## Usage

You can use this to test how many requests per second a server can handle. It will execute concurrent requests in ever incrementing batch sizes defined with the `-i` switch.

Note that the machine making the requests may not be capable of executing requests at the desired speed. So each result shows the *requested* rate and the *achieved rate*. For reference, on an M1 (8 core) Mac Mini on a fibre network, I can barely rach 50rps. On my 32 core server on a fibre network I can occsionally reach 180rps.

The script may also  be ridiculously inefficient. Feel free to make pull requests if you can see how to coax more rps out of weeny machines with feeble network connections.

Note that you might want to see how many concurrent requests per second your machine is able to make before testing it against your target. To do this, I use `example.com`:


```bash
python rps.py -s 20 -e 180 -i 10 -u "https://example.com/" 
```

Then you can test against the API knowing what your machine is capable of.

## Example invocation

```bash
python rps.py  -v  -s 20 -e 50 -i 10
```

## Screenshot

[](.screenshots/1.png)
![screenshot](screenshots/1.png "screenshot")
