
import argparse
import logging
import time
import concurrent.futures
import requests
import re
import os
from collections import Counter
from pprint import pprint
import sys
from rich.console import Console
from rich.table import Table
from rich.progress import track
from rich.logging import RichHandler
from multiprocessing import cpu_count


FORMAT = "%(message)s"
logging.basicConfig(
    level="ERROR", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)

logger = logging.getLogger(__name__)



CR_PRIMARY_RED = "#ef3340"
CR_PRIMARY_GREEN = "#3eb1c8"
CR_PRIMARY_YELLOW = "#ffc72c"
CR_PRIMARY_LT_GREY = "#d8d2c4"
CR_PRIMARY_DK_GREY = "#4f5858"

CR_SECONDARY_RED = "#a6192e"
CR_SECONDARY_GREEN = "#00ab84"
CR_SECONDARY_YELLOW = "#ffa300"
CR_SECONDARY_BLUE = "#005f83"
CR_SECONDARY_LT_GREY = "#a39382"

CR_MONO_1 = "#d9d9d9"
CR_MONO_2 = "#bfbfbf"
CR_MONO_3 = "#a6a6a6"
CR_MONO_4 = "#595959"
CR_MONO_5 = "#333333"

API_HEARTBEAT = "https://api.crossref.org/heartbeat"
USER_AGENT = "crossref-rate-limit-tester"
PLUS_TOKEN_ENV_NAME = 'CROSSREF_API_TOKEN'

def valid_email(polite):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", polite):
        raise ValueError("not an email address")
    return polite

def plus_token():
    return os.environ.get(PLUS_TOKEN_ENV_NAME, None)

def plus_token_exists():
    if not plus_token():
        raise ValueError(f"{PLUS_TOKEN_ENV_NAME} environment variable is not set")
    return True


def headers():
    headers = {}
    if ARGS.polite:
        headers['User-Agent'] = f"{USER_AGENT};mailto:{ARGS.polite}"
    if ARGS.use_plus:
        headers['Crossref-Plus-API-Token'] = plus_token()
    logger.debug(f"headers: {headers}")
    return headers


def get_url(url, headers):
    logger.debug(f"starting request")
    try:
        response = requests.get(url, headers=headers)
    except requests.HTTPError as exc:
        logger.warning(
            f"Error response {exc.response.status_code} while requesting {exc.request.url!r}."
        )
    except requests.exceptions.ConnectionError as err:
        logger.critical(f"connection refused: {err}")
        return f"{err}"
    except Exception as err:
        logger.critical(f"A network error ocurred: {err}")
        return f"{err}"

    logger.debug(f"finished request: ({response.status_code})")
    return response

def get_rate_limit(url,headers):
    response = get_url(url,headers)
    return response.headers.get('x-rate-limit-limit',None)

def get_status_code(url, headers):
    return get_url(url, headers).status_code
    
    
def batch_get(request_rate):
    futures = []
    response_codes = []
    with concurrent.futures.ProcessPoolExecutor(max_workers=200) as executor:
        t0 = time.time()
        for i in range(request_rate):
            future = executor.submit(get_status_code, url=ARGS.url, headers=headers())
            futures.append(future)
        t1 = time.time() - t0
        
        for future in concurrent.futures.as_completed(futures):
            response_codes.append(future.result())
        t2 = time.time() - t0
            
        if t1 > 1: 
            logger.warning(f"Cannot reach rate limit of {request_rate} rps")
            logger.warning(f"Time taken: {t1}")
            
                
    return {'response_codes': response_codes, 'request_time': t1, 'response_time': t2 }


def context_columns():
    table = Table(title=f"Test Context")
    table.add_column("setting", justify="right", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    table.add_column("value", justify="left", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    return table

def summarize_context():
    table = context_columns()
    table.add_row("cpu cores", f"{cpu_count()}")
    table.add_row("advertised rate limit", f"{get_rate_limit(ARGS.url, headers())}")
    table.add_row("start rate", f"{ARGS.start_rate}")
    table.add_row("end rate", f"{ARGS.end_rate}")
    table.add_row("increment rate", f"{ARGS.increment_rate}")
    table.add_row("email", f"{ARGS.polite}")
    table.add_row("plus", f"{ARGS.use_plus}")
    table.add_row("url", f"{ARGS.url}")

    return table

def style_request_result_text(rate, request_time):
    
        if request_time > 1:
            request_time_text = f"[{CR_PRIMARY_RED}]{request_time:.2f}"
            acheived_text = f"[{CR_PRIMARY_RED}]no"    
        else:
            request_time_text = f"{request_time:.2f}"
            acheived_text = f"[{CR_PRIMARY_GREEN}]yes"

        return request_time_text, acheived_text

def results_columns():
    table = Table(title=f"Test Results")
    table.add_column("rate attempted", justify="right", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    table.add_column("achieved", justify="right", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    table.add_column("request_time",  justify="right", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    table.add_column("response_time",  justify="right", style=f"{CR_PRIMARY_LT_GREY}", no_wrap=True)
    table.add_column("success",  justify="right", style=f"{CR_PRIMARY_GREEN}", no_wrap=True)
    table.add_column("rate-limited",  justify="right", style=f"{CR_PRIMARY_YELLOW}", no_wrap=True)
    table.add_column("other",  justify="right", style=f"{CR_PRIMARY_RED}", no_wrap=True)
    return table

def summarize_results(all_results):

    table = results_columns()
    
    for rate, results in all_results.items():
        results_summary = Counter(results['response_codes'])
        request_time_text, achieved_text = style_request_result_text(rate, results['request_time'])
        response_time = results['response_time']
        succeeded = results_summary.get(200,0)
        rate_limited = results_summary.get(429,0)
        other = sum(results_summary.values()) - (succeeded + rate_limited)
        table.add_row(f"{rate}rps", achieved_text, request_time_text, f"{response_time:.2f}",str(succeeded), str(rate_limited),str(other))
    
    return table


def main():
    all_results = {}
  
    for request_rate in track(range(ARGS.start_rate, ARGS.end_rate, ARGS.increment_rate),description="testing...",style=f"{CR_PRIMARY_GREEN}"):
        all_results[request_rate] = batch_get(request_rate)
        time.sleep(1)

    console = Console(color_system="truecolor")
    grid = Table.grid(expand=True)
    grid.add_column()
    grid.add_row(summarize_context())
    grid.add_row(summarize_results(all_results))
    console.print(grid)
     
        
        


if __name__ == "__main__":

    

    parser = argparse.ArgumentParser(
        description="sanity check resource update/transfer files"
    )

    parser.add_argument("-v", "--verbose", help="verbose output", action="store_true",default=False)

    parser.add_argument('-s','--start-rate', type=int, required=False, default=20)
    parser.add_argument('-e','--end-rate', type=int, required=False, default=150)
    parser.add_argument('-i','--increment-rate', type=int, required=False, default=10)
    parser.add_argument('-p','--polite', type=valid_email, required=False, default=None)
    parser.add_argument('--use-plus', action="store_true", default=False)

    parser.add_argument('-u','--url', type=str, required=False, default=API_HEARTBEAT)

    ARGS = parser.parse_args()

    if ARGS.verbose:
        logging.getLogger().setLevel(logging.DEBUG)
        logger.info("verbose mode")

    if ARGS.use_plus:
        try:
            plus_token_exists()
        except ValueError as e:
            logger.critical(e.message)
            sys.exit(1)

    main()
    
    if ARGS.verbose:
        
        logger.info("done")