# Contributing to rate-limit-tester

This project welcomes contributions in the form of Pull Requests.
For clear bug-fixes / typos etc. just submit a PR.
For new features or if there is any doubt in how to fix a bug, you might want
to open an issue prior to starting work, or email `gbilder at crossref dot org`
to discuss it first.

## Development Environment

The tool is written in python3.

