appdirs==1.4.4
astroid==2.6.5
black==21.7b0
certifi==2021.5.30
charset-normalizer==2.0.3
click==8.0.1
colorama==0.4.4
commonmark==0.9.1
humanfriendly==9.2
idna==3.2
isort==5.9.2
lazy-object-proxy==1.6.0
mccabe==0.6.1
mypy-extensions==0.4.3
pathspec==0.9.0
Pygments==2.9.0
pylint==2.9.5
regex==2021.7.6
requests==2.26.0
rich==10.6.0
toml==0.10.2
tomli==1.1.0
urllib3==1.26.6
wrapt==1.12.1
